package br.com.caelum.Factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory
{
    private Connection connection;
    private String url = "jdbc:tipoBanco://localhost:porta/banco";

    public ConnectionFactory setTipoBanco(String tipoBanco)
    {
        url = url.replace("tipoBanco", tipoBanco);
        return this;
    }

    public ConnectionFactory setPorta(String porta)
    {
        url = url.replace("porta", porta);
        return this;
    }
    
    public Connection getConnection()
    {
        try
        {
            connection = DriverManager.getConnection(url, "root", "");
        }
        catch (SQLException err)
        {
            System.out.println("Ocorrência = " + err.getMessage());
        }
        
        return connection;
    }
}
