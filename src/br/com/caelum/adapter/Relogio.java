package br.com.caelum.adapter;

import java.util.Calendar;

public interface Relogio
{
    public Calendar dataDeHoje();
}
