package br.com.caelum.adapter;

import java.util.Calendar;

public class RelogioSistema implements Relogio
{
    @Override
    public Calendar dataDeHoje()
    {
        return Calendar.getInstance();
    }
}
