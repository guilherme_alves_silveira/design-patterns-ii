package br.com.caelum.bridge;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GoogleMaps implements Mapa
{
    private String rua;
    
    public GoogleMaps(){}
    
    public GoogleMaps(String rua)
    {
        this.rua = rua;
    }
    
    @Override
    public String getMapa(String rua)
    {
        return retornaMapa(rua);
    }
    
    public String getMapa()
    {
        return retornaMapa(rua);
    }
    
    private String retornaMapa(String rua)
    {
        try
        {
            String link = "blahblah/" + rua; 
            URL url = new URL(link);
        }
        catch (MalformedURLException ex)
        {
            Logger.getLogger(GoogleMaps.class.getName()).log(Level.SEVERE, null, ex);
        }
        //imagine código
        return "";//imagine que retorne algo válido
    }
}
