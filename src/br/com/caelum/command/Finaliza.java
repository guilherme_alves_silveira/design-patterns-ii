package br.com.caelum.command;

public class Finaliza implements Comando
{
    private Pedido pedido;
    
    public Finaliza(Pedido pedido)
    {
        this.pedido = pedido;
    }

    @Override
    public void executa()
    {
        System.out.println("Pedido de " + pedido.getCliente() + " entregue");
        pedido.finaliza();
    }
}
