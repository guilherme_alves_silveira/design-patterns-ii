package br.com.caelum.command;

import java.util.ArrayList;
import java.util.List;

public class ListaDeComandos
{
    private List<Comando> comandos;
    
    public ListaDeComandos()
    {
        this.comandos = new ArrayList<>();
    }
    
    public void adiciona(Comando comando)
    {
        comandos.add(comando);
    }
    
    public void processa()
    {
        for(Comando comando : comandos)
        {
            comando.executa();
        }
    }
}
