package br.com.caelum.command;

public class Paga implements Comando
{
    private final Pedido pedido;
    
    public Paga(Pedido pedido)
    {
        this.pedido = pedido;
    }
    
    @Override
    public void executa()
    {
        System.out.println("Pedido de " + pedido.getCliente() + " pago");
        pedido.paga();
    }
}
