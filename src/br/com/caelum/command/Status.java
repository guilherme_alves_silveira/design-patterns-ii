package br.com.caelum.command;

public enum Status
{
    NOVO,
    PROCESSANDO,
    PAGO,
    ITEM_SEPARADO,
    ENTREGUE;
}
