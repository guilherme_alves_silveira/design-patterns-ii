package br.com.caelum.command;

public class TestaComando
{
    public static void main(String[] args)
    {
        Pedido p1 = new Pedido("Guilherme", 100.00);
        Pedido p2 = new Pedido("Jean", 400.00);
        
        Comando paga1 = new Paga(p1);
        Comando paga2 = new Paga(p2);
        
        Comando fin1 = new Finaliza(p1);
        Comando fin2 = new Finaliza(p2);
        
        ListaDeComandos lista = new ListaDeComandos();
        
        lista.adiciona(paga1);
        lista.adiciona(paga2);
        lista.adiciona(fin1);
        lista.adiciona(fin2);
        
        lista.processa();
    }
}
