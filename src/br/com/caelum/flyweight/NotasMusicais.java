package br.com.caelum.flyweight;

import br.com.caelum.flyweight.notas.Do;
import br.com.caelum.flyweight.notas.DoSustenido;
import br.com.caelum.flyweight.notas.Fa;
import br.com.caelum.flyweight.notas.La;
import br.com.caelum.flyweight.notas.Mi;
import br.com.caelum.flyweight.notas.Re;
import br.com.caelum.flyweight.notas.ReSustenido;
import br.com.caelum.flyweight.notas.Si;
import br.com.caelum.flyweight.notas.Sol;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotasMusicais
{
    private static Map<String, Nota> notas = new HashMap<>();
    private static List<Class<? extends Nota>> clazzes;
    
    static
    {
        clazzes = Arrays.asList
                (Do.class, Re.class, Mi.class,
                 Fa.class, Sol.class, La.class, Si.class,
                 DoSustenido.class, ReSustenido.class
                );
    }
    
    public Nota pega(String nome)
    {
        try
        {
            if(!notas.containsKey(nome))
            {
                for(Class<? extends Nota> clazz : clazzes)
                {
                    if(clazz.getSimpleName().toLowerCase().equals(nome))
                    {
                        notas.put(nome, (Nota) clazz.newInstance());
                        break;
                    }
                }
            }
            
            return notas.get(nome);
        }
        catch (IllegalAccessException | InstantiationException e)
        {
            throw new RuntimeException(e);
        }
    }
}
