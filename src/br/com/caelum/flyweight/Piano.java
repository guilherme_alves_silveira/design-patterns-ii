package br.com.caelum.flyweight;

import java.util.List;
import org.jfugue.Player;

public class Piano
{
    public void toca(List<Nota> notasMusicais)
    {
        
        StringBuilder notas = new StringBuilder();
        
        for(Nota nota : notasMusicais)
        {
            notas.append(nota.simbolo()).append(" ");
        }
        
        Player tocador = new Player();
        tocador.play(notas.toString());
    }
}
