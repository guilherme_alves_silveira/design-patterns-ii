package br.com.caelum.flyweight;

import java.util.Arrays;
import java.util.List;

public class Programa
{
    private static final NotasMusicais musicas = new NotasMusicais();
    
    public static void main(String[] args)
    {
        List<Nota> musica = Arrays.asList
                                        (
                                            musicas.pega("do"),
                                            musicas.pega("re"),
                                            musicas.pega("mi"),
                                            musicas.pega("fa"),
                                            musicas.pega("fa"),
                                            musicas.pega("fa"),
                                            musicas.pega("sol")
                                        );
        
        Piano piano = new Piano();
        piano.toca(musica);
    }
}
