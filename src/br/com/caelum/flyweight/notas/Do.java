package br.com.caelum.flyweight.notas;

import br.com.caelum.flyweight.Nota;

public class Do implements Nota
{
    @Override
    public String simbolo()
    {
        return "C";
    }
}
