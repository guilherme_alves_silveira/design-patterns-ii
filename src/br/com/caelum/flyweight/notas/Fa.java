package br.com.caelum.flyweight.notas;

import br.com.caelum.flyweight.Nota;

public class Fa implements Nota
{
    @Override
    public String simbolo()
    {
        return "F";
    }
}
