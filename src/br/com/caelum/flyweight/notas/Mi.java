package br.com.caelum.flyweight.notas;

import br.com.caelum.flyweight.Nota;

public class Mi implements Nota
{
    @Override
    public String simbolo()
    {
        return "E";
    }
}
