package br.com.caelum.flyweight.notas;

import br.com.caelum.flyweight.Nota;

public class Si implements Nota
{
    @Override
    public String simbolo()
    {
        return "B";
    }
}
