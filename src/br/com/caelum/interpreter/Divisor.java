package br.com.caelum.interpreter;

import br.com.caelum.interpreter.visitor.Visitor;

public class Divisor implements Expressao, Lado
{
    private Expressao esquerda;
    private Expressao direita;
    
    public Divisor(Expressao esquerda, Expressao direita)
    {
        this.esquerda = esquerda;
        this.direita = direita;
    }

    @Override
    public double avalia()
    {
        if(esquerda.avalia() == 0)  return 0;
        if(direita.avalia() == 0) throw new IllegalArgumentException("Impossível divisão por zero.");
        return  esquerda.avalia() / direita.avalia();
    }
    
    @Override
    public void aceita(Visitor visitor)
    {
        visitor.imprime(this, this);
    }

    @Override
    public String tipo()
    {
        return "/";
    }

    @Override
    public Expressao getEsquerda()
    {
        return this.esquerda;
    }

    @Override
    public Expressao getDireita()
    {
        return this.direita;
    }
}
