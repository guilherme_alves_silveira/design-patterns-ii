package br.com.caelum.interpreter;

import br.com.caelum.interpreter.visitor.Visitor;

public interface Expressao
{
    public double avalia();
    public void aceita(Visitor visitor);
    public String tipo();
}
