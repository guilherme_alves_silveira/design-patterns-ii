package br.com.caelum.interpreter;

public interface Lado
{
    public Expressao getEsquerda();
    public Expressao getDireita();    
}
