package br.com.caelum.interpreter;

import br.com.caelum.interpreter.visitor.Visitor;

public class Multiplicador implements Expressao, Lado
{
    private Expressao esquerda;
    private Expressao direita;
    
    public Multiplicador(Expressao esquerda, Expressao direita)
    {
        this.esquerda = esquerda;
        this.direita = direita;
    }
    
    @Override
    public double avalia()
    {
        return esquerda.avalia() * direita.avalia();
    }

    @Override
    public void aceita(Visitor visitor)
    {
        visitor.imprime(this, this);
    }

    @Override
    public String tipo()
    {
        return "*";
    }
    
    @Override
    public Expressao getEsquerda()
    {
        return this.esquerda;
    }

    @Override
    public Expressao getDireita()
    {
        return this.direita;
    }
}
