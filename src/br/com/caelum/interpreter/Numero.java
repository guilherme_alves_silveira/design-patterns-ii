package br.com.caelum.interpreter;

import br.com.caelum.interpreter.visitor.Visitor;

public class Numero implements Expressao
{
    private double numero;
    
    public Numero(double numero)
    {
        this.numero = numero;
    }

    @Override
    public double avalia()
    {
        return numero;
    }

    @Override
    public void aceita(Visitor visitor)
    {
        visitor.imprime(this);
    }

    @Override
    public String tipo()
    {
        return "";
    }
}
