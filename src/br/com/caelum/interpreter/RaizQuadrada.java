package br.com.caelum.interpreter;

import br.com.caelum.interpreter.visitor.Visitor;

public class RaizQuadrada implements Expressao 
{
    private Expressao expressao;
    
    public RaizQuadrada(Expressao expressao)
    {
        this.expressao = expressao;
    }

    @Override
    public double avalia()
    {
        return Math.sqrt(expressao.avalia());
    }

    @Override
    public void aceita(Visitor visitor)
    {
        visitor.imprime(this);
    }

    @Override
    public String tipo()
    {
        return "";
    }
}
