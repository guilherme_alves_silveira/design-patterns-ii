package br.com.caelum.interpreter;

import br.com.caelum.interpreter.visitor.Impressora;
import br.com.caelum.interpreter.visitor.OutraImpressora;
import br.com.caelum.interpreter.visitor.Visitor;

public class TestaInterpreter
{
    public static void main(String[] args)
    {
        Expressao soma = new Soma(new Numero(10), new Subtracao(new Numero(15), new Soma(new Numero(5), new Numero(30))));
        double resultado = soma.avalia();
        System.out.println("Resultado: " + resultado);
        Visitor impressora = new Impressora();
        Visitor outraImpressora = new OutraImpressora();
        
        soma.aceita(impressora);
        System.out.println("\n");
        soma.aceita(outraImpressora);
        System.out.println("\n");
    }
}
