package br.com.caelum.interpreter.visitor;

import br.com.caelum.interpreter.Expressao;

public class Impressora extends TemplateImpressao implements Visitor
{
    @Override
    public void imprime(Expressao expressao)
    {
        System.out.print(expressao.avalia());
    }
}
