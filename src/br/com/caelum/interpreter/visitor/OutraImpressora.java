package br.com.caelum.interpreter.visitor;

import br.com.caelum.interpreter.Expressao;

public class OutraImpressora extends TemplateOutraImpressora
{
    @Override
    public void imprime(Expressao expressao)
    {
        System.out.print(expressao.avalia());
    }
}
