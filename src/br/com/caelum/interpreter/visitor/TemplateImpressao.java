package br.com.caelum.interpreter.visitor;

import br.com.caelum.interpreter.Expressao;
import br.com.caelum.interpreter.Lado;

public abstract class TemplateImpressao implements Visitor
{
    @Override
    public void imprime(Expressao expressao, Lado lado)
    {
        System.out.print("( ");
        
        lado.getEsquerda().aceita(this);
        
        System.out.print(" " + expressao.tipo() + " ");
        
        lado.getDireita().aceita(this);
        
        System.out.print(" )");
    }
}
