package br.com.caelum.interpreter.visitor;

import br.com.caelum.interpreter.Expressao;
import br.com.caelum.interpreter.Lado;

public abstract class TemplateOutraImpressora implements Visitor
{
    @Override
    public void imprime(Expressao expressao, Lado lado)
    {
        System.out.print(expressao.tipo() + " ");
        
        lado.getEsquerda().aceita(this);
            
        System.out.println(" ");
        
        lado.getDireita().aceita(this);
    }
}
