package br.com.caelum.interpreter.visitor;

import br.com.caelum.interpreter.Expressao;
import br.com.caelum.interpreter.Lado;

public interface Visitor
{
    public void imprime(Expressao expressao, Lado lado);
    public void imprime(Expressao expressao);
}
