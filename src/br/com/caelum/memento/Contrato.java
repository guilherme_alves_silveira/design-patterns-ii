package br.com.caelum.memento;

import java.util.Calendar;

public class Contrato
{
    private Calendar data;
    private String nomeCliente;
    private TipoContrato tipo;
    
    public Contrato(Calendar data, String nomeCliente, TipoContrato tipo)
    {
        this.data = data;
        this.nomeCliente = nomeCliente;
        this.tipo = tipo;
    }
    
    public Calendar getData()
    {
        return data;
    }

    public String getNomeCliente()
    {
        return nomeCliente;
    }

    public TipoContrato getTipo()
    {
        return tipo;
    }
    
    public void avanca()
    {
        if(tipo == TipoContrato.NOVO) tipo = TipoContrato.EM_ANDAMENTO;
        else if(tipo == TipoContrato.EM_ANDAMENTO) tipo = TipoContrato.ACERTADO;
        else if(tipo == TipoContrato.ACERTADO) tipo = TipoContrato.CONCLUIDO;
    }
    
    public Contrato getEstadoAtual()
    {
        return new Contrato(this.data, this.nomeCliente, this.tipo);
    }
}
