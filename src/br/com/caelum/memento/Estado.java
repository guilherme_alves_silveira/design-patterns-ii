package br.com.caelum.memento;

public class Estado
{
    private Contrato contrato;
    
    public Estado(Contrato contrato)
    {
        this.contrato = contrato.getEstadoAtual();
    }
    
    public Estado getEstado()
    {
        return this;
    }
    
    public Contrato getContrado()
    {
        return contrato;
    }
}
