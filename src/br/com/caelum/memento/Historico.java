package br.com.caelum.memento;

import java.util.ArrayList;
import java.util.List;

public class Historico
{
    private List<Estado> estados = new ArrayList<>();
    
    public void adiciona(Estado estado)
    {
        if(estado != null && estado.getContrado() != null)
        {
            this.estados.add(estado);
        }
    }
    
    public Estado pega(int index)
    {
        if(index >= 0)
        {
            return this.estados.get(index);
        }
        else
        {
            throw new IllegalArgumentException("O indíce deve ser maior ou igual a zero.");
        }
    }
}
