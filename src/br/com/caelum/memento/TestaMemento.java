package br.com.caelum.memento;

import java.util.Calendar;

public class TestaMemento
{
    public static void main(String[] args)
    {
        Contrato c1 = new Contrato(Calendar.getInstance(),"Guilherme", TipoContrato.NOVO);
        Estado estado = new Estado(c1);
        
        System.out.println(c1.getTipo());
        
        Historico historico = new Historico();
        historico.adiciona(estado);
        
        c1.avanca();
        Estado outroEstado = new Estado(c1);
        System.out.println(outroEstado.getContrado());
        historico.adiciona(outroEstado);
        
        System.out.println("Histórico 1:" + historico.pega(0).getContrado().getTipo());
        System.out.println("Histórico 2:" + historico.pega(1).getContrado().getTipo());
    }
}
