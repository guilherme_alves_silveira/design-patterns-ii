package br.com.caelum.memento;

public enum TipoContrato
{
    NOVO, 
    EM_ANDAMENTO,
    ACERTADO,
    CONCLUIDO
}
